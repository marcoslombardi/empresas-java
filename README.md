# README #

Projeto Empresas para o Processo Seletivo ioasys - Teste Back-end Java

### Tecnologias ###

Projeto criado usando Knappsack Maven Archetypes. 
Para mais informações acesse: https://www.andygibson.net/blog/projects/knappsack/

Utiliza Maven, Java 1.8, JPA, Hibernate, CDI, JSF, Facelets, Omnifaces, Primefaces com servidor de aplicação Wildfly-8.2.1 e banco de dados Postgres 4.20

### Funcionalidades ###

* Tela de login/autenticação de usuario
* CRUD - Empresas

### Como faço para configurar/deploy? ###

* Instale o Java SE Development Kit 8 e configure a variável de ambiente JAVA_HOME
* Instale o banco de dados PostgreSQL 4.20 com senha 'postHPv185es' para user root e crie uma instancia com o nome ioasys no pgAdmin do PostgreSQL 
* Baixe e configure o Maven-3.6.3 ou superior
* Baixe o servidor de aplicação Wildfly-8.2.1 e configure a variável de ambiente JBOSS_HOME
* Execute o comando Maven: mvn install
* Copie a aplicação gerada na pasta target\ioasysMyApp.war para JBOSS_HOME\standalone\deployments
* Execute JBOSS_HOME\bin\standalone.bat ou passo 3 - Outras informações

### Configurando datasource BD e driver JDBC Postgres ###

* Criar a estrutura de pastas em JBOSS_HOME\modules\org\postgresql\main
* Copiar o drive JDBC localizado em main\resources\postgresql-42.2.5.jre7.jar para a pasta acima
* Crie um arquivo com nome module.xml na mesma pasta e conteúdo abaixo

			<?xml version="1.0" encoding="UTF-8"?>
			  <!-- se quiser alterar o name abaixo, lembre de alterar no arquivo standalone depois -->
			  <module xmlns="urn:jboss:module:1.0" name="org.postgresql">
			  <resources>
				<!-- faça as alterações necessárias para o seu driver e versão específico -->
				<resource-root path="postgresql-42.2.5.jre7.jar"/>
			  </resources>
			  <dependencies>
				<module name="javax.api"/>
				<module name="javax.transaction.api"/>
			  </dependencies>
			</module>

* Adicione o Datasource abaixo no arquivo JBOSS_HOME\standalone\configuration\standalone.xml
  
                <datasource jndi-name="java:/ioasysDS" pool-name="ioasysDS" enabled="true" use-java-context="true">
                    <connection-url>jdbc:postgresql:ioasys</connection-url>
                    <driver>org.postgresql</driver>
                    <transaction-isolation>TRANSACTION_READ_COMMITTED</transaction-isolation>
                    <pool>
                        <min-pool-size>10</min-pool-size>
                        <max-pool-size>100</max-pool-size>
                        <prefill>true</prefill>
                    </pool>
                    <security>
                        <user-name>postgres</user-name>
                        <password>postHPv185es</password>
                    </security>
                    <statement>
                        <prepared-statement-cache-size>32</prepared-statement-cache-size>
                        <share-prepared-statements>true</share-prepared-statements>
                    </statement>
                </datasource>


### Outras informações ###

 As tabelas do banco de dados são criadas no deploy da aplicação e preenchidas utilizando a lib DataFactory para geração de dados fictícios/aleatório

 Depois que o aplicativo é empacotado (.war) e implantado, ele pode ser visualizado usando o seguinte URL:
 
 http://localhost:8080/ioasysMyApp/index.jsf
 
 Este aplicativo pode ser implementado em qualquer contêiner Java EE por meio do
 métodos a seguir
 
 1) Integração IDE - Qualquer IDE compatível com Maven (como Netbeans ou Eclipse)
    deve ser capaz de implantar este aplicativo usando métodos fornecidos por
    o IDE
    
 2) Manualmente - Se você executar o comando Maven:
 
     mvn clean package
    
    O Maven irá limpar, construir e empacotar o aplicativo e você pode implante manualmente o arquivo re {project.artifactId} et / $ {project.artifactId} .war em seu servidor
    
 3) Você pode implantar no JBoss se tiver a variável de ambiente JBOSS_HOME configurar usando:
    
    mvn clean package jboss:hard-deploy
    
    Isso irá limpar, construir e empacotar o aplicativo e, em seguida, copiar o aplicativo para o diretório jboss deploy para a instalação do JBoss definido por JBOSS_HOME.